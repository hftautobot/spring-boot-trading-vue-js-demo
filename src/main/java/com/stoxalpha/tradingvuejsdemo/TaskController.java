package com.stoxalpha.tradingvuejsdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskController extends Task {

	@Autowired
	private TaskRepository taskRepository;

	@PostMapping(path = "/api/task")
	public void addTask(@RequestBody Task task) {
		taskRepository.save(task);
	}

	@GetMapping(path = "/api/task/{id}")
	public Task getTask(@PathVariable long id) {
		return taskRepository.getOne(id);
	}

	@DeleteMapping(path = "/api/task/{id}")
	public void deleteTask(@PathVariable long id) {
		taskRepository.deleteById(id);
	}

	@GetMapping(path = "/api/tasks")
	public List<Task> getTasks() {
		return taskRepository.findAll();
	}
}
