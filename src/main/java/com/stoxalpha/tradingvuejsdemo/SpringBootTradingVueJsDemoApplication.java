package com.stoxalpha.tradingvuejsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTradingVueJsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTradingVueJsDemoApplication.class, args);
    }

}
